/**
 * Include your custom JavaScript here.
 *
 * We also offer some hooks so you can plug your own logic. For instance, if you want to be notified when the variant
 * changes on product page, you can attach a listener to the document:
 *
 * document.addEventListener('variant:changed', function(event) {
 *   var variant = event.detail.variant; // Gives you access to the whole variant details
 * });
 *
 * You can also add a listener whenever a product is added to the cart:
 *
 * document.addEventListener('product:added', function(event) {
 *   var variant = event.detail.variant; // Get the variant that was added
 *   var quantity = event.detail.quantity; // Get the quantity that was added
 * });
 *
 * If you just want to force refresh the mini-cart without adding a specific product, you can trigger the event
 * "cart:refresh" in a similar way (in that case, passing the quantity is not necessary):
 *
 * document.documentElement.dispatchEvent(new CustomEvent('cart:refresh', {
 *   bubbles: true
 * }));
 */




  document.addEventListener('variant:changed', function(event) {
    var variant = event.detail.variant; 
    
    var fmediID = variant.featured_media.id;
    $('#main-prod-slider').slick('slickGoTo',$('#main-prod-slider #Media'+fmediID).data('slickIndex'));
    var color_handle  = variant.option1;
    color_handle = color_handle.toLowerCase().replace(/ /g,'-');
    color_handle = '.'+color_handle;
    
    $('#thumb-slider').slick('slickUnfilter');
    $('#thumb-slider').slick('slickFilter',color_handle);
    
    $('#main-prod-mobile-slider').slick('slickUnfilter');
    $('#main-prod-mobile-slider').slick('slickFilter',color_handle);                         
 
    
  });

function toggleUpsellItem(element,vid,lineID){
  if(! lineID){
    dt = {
      "quantity": 1,
      "id": vid
    }
    
    $.ajax({
      url: "../../../cart/add.js",
      data: dt,
      type : 'POST',
      dataType:'json',
      success: function(result){
        location.reload();

      },
      error:function(result){
        console.log("Error");
      }
    });
  }else{
    var quantity =  0;
    $.ajax({
      type: 'POST',
      url: '/cart/change.js',
      data: 'quantity=' + quantity + '&id=' + lineID,
      dataType: 'json',
      success: function(line_item) { 
        location.reload();       
      },
      error: function(XMLHttpRequest, textStatus) {
        console.log("Error");
      }
    });
  }
}

jQuery('[data-action="open-drawer"]').click(function(){
  $(".shipping-pro").html('');
  $.ajax({
    url: '/cart.js',
    dataType:  'json',
    success: function(result){
    },
    complete: function(result){
      var item = result.responseJSON.items.find(x => x.variant_id == "40387992715455");
      if(item){
        $(".shipping-pro").html('<div class="section shipping-protection"><div class="rj-left"><img src="https://cdn.shopify.com/s/files/1/0058/7776/0088/products/primeinsuredversion2_31x.png"></div><span class="rj-medium"> <b>Shipping Protection</b><br>from Damage, Loss & Theft</span><span class="rw-checkbox-span rw-checked"><span class="rw-slider rw-checked"></span><span class="rw-on-text rw-checked">On</span><span class="rw-off-text rw-checked">Off</span></span></div><div class="section desc"><p>Superior protection that provides quick and worry free package replacement for damaged, lost, or stolen orders, as well as getting your order shipped out next business day!</p></div>')
      }else{
        $(".shipping-pro").html('<div class="section shipping-protection"><div class="rj-left"><img src="https://cdn.shopify.com/s/files/1/0058/7776/0088/products/primeinsuredversion2_31x.png"></div><span class="rj-medium"> <b>Shipping Protection</b><br>from Damage, Loss & Theft</span><span class="rw-checkbox-span rw-unchecked"><span class="rw-slider rw-unchecked"></span><span class="rw-on-text rw-unchecked">On</span><span class="rw-off-text rw-unchecked">Off</span></span></div><div class="section desc"><p>Superior protection that provides quick and worry free package replacement for damaged, lost, or stolen orders, as well as getting your order shipped out next business day!</p></div>')
      }
    }
  });
});

$(document).on('click', '.rw-checked', function(){
  $(".rw-checked").removeClass('rw-checked').addClass('rw-unchecked');
  $.ajax({
    url: '/cart/change.js',
    type: 'post',
    data: {id:'40387992715455',quantity:0},
    success: function(result){
      //window.location.reload()
    },
    complete: function(result){
//       window.location.reload()
       document.documentElement.dispatchEvent(new CustomEvent('cart:refresh', {
        bubbles: true
      }));
    }
  });
});

$(document).on('click', '.rw-unchecked', function(){
  $(".rw-unchecked").removeClass('rw-unchecked').addClass('rw-checked');
  $.ajax({
    url: '/cart/add.js',
    type: 'post',
    data: {id:'40387992715455'},
    success: function(result){
      //window.location.reload()
      console.log(result);
    },
    complete: function(result){
//       window.location.reload()
       document.documentElement.dispatchEvent(new CustomEvent('cart:refresh', {
        bubbles: true
      }));
    }
  });
});

setInterval(function(){ 
  $('.Drawer__Main h2.CartItem__Title.Heading a').each(function(){
      var $this = $(this)
      if($this.text() == 'Prime Insured Shipping'){
          $(this).parents('.CartItemWrapper').addClass('last_order')
      }
  })
if($('.rj-left')[0]){}else{
    if($('html').hasClass('no-scroll')){
//         $(".shipping-pro").html('');
  $.ajax({
    url: '/cart.js',
    dataType:  'json',
    success: function(result){
    },
    complete: function(result){
      var item = result.responseJSON.items.find(x => x.variant_id == "40387992715455");
      if(item){
        $(".shipping-pro").html('<div class="section shipping-protection"><div class="rj-left"><img src="https://cdn.shopify.com/s/files/1/0058/7776/0088/products/primeinsuredversion2_31x.png"></div><span class="rj-medium"> <b>Shipping Protection</b><br>from Damage, Loss & Theft</span><span class="rw-checkbox-span rw-checked"><span class="rw-slider rw-checked"></span><span class="rw-on-text rw-checked">On</span><span class="rw-off-text rw-checked">Off</span></span></div><div class="section desc"><p>Superior protection that provides quick and worry free package replacement for damaged, lost, or stolen orders, as well as getting your order shipped out next business day!</p></div>')
      }else{
        $(".shipping-pro").html('<div class="section shipping-protection"><div class="rj-left"><img src="https://cdn.shopify.com/s/files/1/0058/7776/0088/products/primeinsuredversion2_31x.png"></div><span class="rj-medium"> <b>Shipping Protection</b><br>from Damage, Loss & Theft</span><span class="rw-checkbox-span rw-unchecked"><span class="rw-slider rw-unchecked"></span><span class="rw-on-text rw-unchecked">On</span><span class="rw-off-text rw-unchecked">Off</span></span></div><div class="section desc"><p>Superior protection that provides quick and worry free package replacement for damaged, lost, or stolen orders, as well as getting your order shipped out next business day!</p></div>')
      }
    }
  });
    }
}
}, 100);

$('button.ProductForm__AddToCart').click(function(){
	var checkExist = setInterval(function() {
    if ($('.rj-left'). length) {
      console.log("Exists!" );
      $('.rw-unchecked').click()
      clearInterval(checkExist);
    }
    }, 100); 
})
var checkExist2 = setInterval(function() {
  if(jQuery('.tech_image .tmenu_image img').length) {
    var src = jQuery(".tech_image .tmenu_image img").data("srcset");
    var array = src.split(",");
    var img = array[array.length-1];
    var array2 = img.split("_2048");
    var first = array2[0];
    var array3 = array2[1].split("@");
    var array4 = array3[1].split("?");
    var img_url = first+"_540x335@"+array4[0];
    jQuery(".tech_image .tmenu_image img").attr("srcset",img_url);
    clearInterval(checkExist2);
  }
}, 100);
var checkExist3 = setInterval(function() {
  if(jQuery('.replace_gif .tmenu_image img').length) {
      var src = jQuery(".replace_gif .tmenu_image img").data("srcset");
      var array = src.split(",");
      var img = array[array.length-1];
      var array2 = img.split("_2048");
      var first = array2[0];
      var array3 = array2[1].split("@");
      var array4 = array3[1].split("?");
      var img_url = first+"@"+array4[0];
      jQuery(".replace_gif .tmenu_image img").attr("srcset",img_url);
      clearInterval(checkExist3);
  }
}, 100);


$(document).ready(function() {
  
  var width = $(window).width();
	
  $(".shopify-section.megamenu").each(function() {
    
    var $menu, $target, $parent;
    
    if(width > 750) {
      $menu = $(this).find('[data-megamenu]');
      $parent = $('header .Header__Wrapper li[data-parent="' + $menu.attr('data-megamenu') + '"]');
	  $target = $parent.find('div.MegaMenu');
    }
    else {
      $menu = $(this).find('[data-mob-megamenu]');
      $parent = $('.SidebarMenu__Nav > div.Collapsible[data-mob-parent="' + $menu.attr('data-mob-megamenu') + '"]');
      $target = $parent.find('.Collapsible__Inner');
    }
    
    
    if($target.length > 0) {
      $target.html($menu.html());
      $parent.addClass('has-megamenu');
    }
    
  })

  $(document).on('click', '.video-popup-open', function() {
    $('.video-popup-wrapper').addClass('open');
  })
  $(document).on('click', '.popup_close', function() {
    $('.video-popup-wrapper').removeClass('open');
  })
  
})